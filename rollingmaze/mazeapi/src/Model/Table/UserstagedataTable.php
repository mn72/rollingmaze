<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Userstagedata Model
 *
 * @method \App\Model\Entity\Userstagedata get($primaryKey, $options = [])
 * @method \App\Model\Entity\Userstagedata newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Userstagedata[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Userstagedata|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Userstagedata saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Userstagedata patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Userstagedata[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Userstagedata findOrCreate($search, callable $callback = null, $options = [])
 */
class UserstagedataTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('userstagedata');
        $this->setDisplayField('UserId');
        $this->setPrimaryKey('UserId');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('UserId')
            ->maxLength('UserId', 8)
            ->allowEmptyString('UserId', null, 'create');

        $validator
            ->integer('StageNum')
            ->allowEmptyString('StageNum');

        $validator
            ->scalar('LatestClearTime')
            ->maxLength('LatestClearTime', 128)
            ->allowEmptyString('LatestClearTime');

        $validator
            ->scalar('FastestClearTime')
            ->maxLength('FastestClearTime', 128)
            ->allowEmptyString('FastestClearTime');

        $validator
            ->integer('ClearCount')
            ->allowEmptyString('ClearCount');

        return $validator;
    }
}
