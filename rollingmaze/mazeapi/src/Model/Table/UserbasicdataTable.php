<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Userbasicdata Model
 *
 * @method \App\Model\Entity\Userbasicdata get($primaryKey, $options = [])
 * @method \App\Model\Entity\Userbasicdata newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Userbasicdata[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Userbasicdata|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Userbasicdata saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Userbasicdata patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Userbasicdata[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Userbasicdata findOrCreate($search, callable $callback = null, $options = [])
 */
class UserbasicdataTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('userbasicdata');
        $this->setDisplayField('Id');
        $this->setPrimaryKey('Id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('Id')
            ->maxLength('Id', 8)
            ->allowEmptyString('Id', null, 'create');

        $validator
            ->scalar('Name')
            ->maxLength('Name', 128)
            ->allowEmptyString('Name');

        $validator
            ->dateTime('RegistrationDate')
            ->notEmptyDateTime('RegistrationDate');

        $validator
            ->dateTime('LoginDate')
            ->notEmptyDateTime('LoginDate');

        $validator
            ->scalar('Password')
            ->maxLength('Password', 128)
            ->allowEmptyString('Password');

        return $validator;
    }
}
