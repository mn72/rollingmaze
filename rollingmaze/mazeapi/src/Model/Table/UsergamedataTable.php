<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Usergamedata Model
 *
 * @method \App\Model\Entity\Usergamedata get($primaryKey, $options = [])
 * @method \App\Model\Entity\Usergamedata newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Usergamedata[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Usergamedata|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Usergamedata saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Usergamedata patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Usergamedata[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Usergamedata findOrCreate($search, callable $callback = null, $options = [])
 */
class UsergamedataTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('usergamedata');
        $this->setDisplayField('UserId');
        $this->setPrimaryKey('UserId');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('UserId')
            ->maxLength('UserId', 8)
            ->allowEmptyString('UserId', null, 'create');

        $validator
            ->integer('StageClearStatus')
            ->allowEmptyString('StageClearStatus');

        $validator
            ->scalar('Difficulty')
            ->maxLength('Difficulty', 16)
            ->requirePresence('Difficulty', 'create')
            ->notEmptyString('Difficulty');

        $validator
            ->numeric('Speed')
            ->requirePresence('Speed', 'create')
            ->notEmptyString('Speed');

        $validator
            ->integer('SkinIdentification')
            ->allowEmptyString('SkinIdentification');

        $validator
            ->integer('Coin')
            ->requirePresence('Coin', 'create')
            ->notEmptyString('Coin');

        return $validator;
    }
}
