<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Userdata Model
 *
 * @method \App\Model\Entity\Userdata get($primaryKey, $options = [])
 * @method \App\Model\Entity\Userdata newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Userdata[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Userdata|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Userdata saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Userdata patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Userdata[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Userdata findOrCreate($search, callable $callback = null, $options = [])
 */
class UserdataTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('userdata');
        $this->setDisplayField('Id');
        $this->setPrimaryKey('Id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('Id')
            ->maxLength('Id', 8)
            ->allowEmptyString('Id', null, 'create');

        $validator
            ->scalar('Name')
            ->maxLength('Name', 128)
            ->requirePresence('Name', 'create')
            ->notEmptyString('Name');

        $validator
            ->dateTime('RagistrationDate')
            ->notEmptyDateTime('RagistrationDate');

        $validator
            ->dateTime('LoginDate')
            ->notEmptyDateTime('LoginDate');

        $validator
            ->integer('StageClearStatus')
            ->notEmptyString('StageClearStatus');

        $validator
            ->scalar('LatestClearTime')
            ->maxLength('LatestClearTime', 128)
            ->allowEmptyString('LatestClearTime');

        $validator
            ->scalar('FastestClearTime')
            ->maxLength('FastestClearTime', 128)
            ->allowEmptyString('FastestClearTime');

        $validator
            ->integer('ClearCount')
            ->notEmptyString('ClearCount');

        return $validator;
    }
}
