<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Usergamedata Entity
 *
 * @property string $UserId
 * @property int|null $StageClearStatus
 * @property string $Difficulty
 * @property float $Speed
 * @property int|null $SkinIdentification
 * @property int $Coin
 */
class Usergamedata extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'StageClearStatus' => true,
        'Difficulty' => true,
        'Speed' => true,
        'SkinIdentification' => true,
        'Coin' => true
    ];
}
