<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Userbasicdata Controller
 *
 * @property \App\Model\Table\UserbasicdataTable $Userbasicdata
 *
 * @method \App\Model\Entity\Userbasicdata[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserbasicdataController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $userbasicdata = $this->paginate($this->Userbasicdata);

        $this->set(compact('userbasicdata'));
    }

    /**
     * View method
     *
     * @param string|null $id Userbasicdata id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userbasicdata = $this->Userbasicdata->get($id, [
            'contain' => []
        ]);

        $this->set('userbasicdata', $userbasicdata);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userbasicdata = $this->Userbasicdata->newEntity();
        if ($this->request->is('post')) {
            $userbasicdata = $this->Userbasicdata->patchEntity($userbasicdata, $this->request->getData());
            if ($this->Userbasicdata->save($userbasicdata)) {
                $this->Flash->success(__('The userbasicdata has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The userbasicdata could not be saved. Please, try again.'));
        }
        $this->set(compact('userbasicdata'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Userbasicdata id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userbasicdata = $this->Userbasicdata->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userbasicdata = $this->Userbasicdata->patchEntity($userbasicdata, $this->request->getData());
            if ($this->Userbasicdata->save($userbasicdata)) {
                $this->Flash->success(__('The userbasicdata has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The userbasicdata could not be saved. Please, try again.'));
        }
        $this->set(compact('userbasicdata'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Userbasicdata id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userbasicdata = $this->Userbasicdata->get($id);
        if ($this->Userbasicdata->delete($userbasicdata)) {
            $this->Flash->success(__('The userbasicdata has been deleted.'));
        } else {
            $this->Flash->error(__('The userbasicdata could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getUserbasicdata()
    {
        error_log("getUserbasicdata");
        $this->autoRender = false;

        $query = $this->Userbasicdata->find('all');

        $json_array = json_encode($query);

        echo $json_array;
    }

    public function addrecordUserbasicdata()
    {
        error_log("addrecordUserbasicdata");
        $this->autoRender = false;
        $userid = "";
        if(isset($this->request->data['userid']))
        {
            $userid = $this->request->data['userid'];
            error_log($userid);
        }
        $username = "";
        if(isset($this->request->data['username']))
        {
            $username = $this->request->data['username'];
            error_log($username);
        }
        $password = "";
        if(isset($this->request->data['password']))
        {
            $username = $this->request->data['password'];
            error_log($username);
        }
       $data = array('Id'=>$userid,'Name'=>$username,'RegistrationDate'=>date('Y/m/d H:i:s'),'LoginDate'=>date('Y/m/d H:i:s'),'Password'=>$password);
       $userbasicdata = $this->Userbasicdata->newEntity();
       $userbasicdata = $this->Userbasicdata->patchEntity($userbasicdata,$data);

       if($this->Userbasicdata->save($userbasicdata))
       {
           echo "1";
       }
       else 
       {
           echo "0";
       }
    }

    public function updateUserbasicdata()
    {
        error_log("updateUserbasicdata");
        $this->autoRender = false;

        $username = "";
        if(isset($this->request->data['username']))
        {
            $username = $this->request->data['username'];
            error_log($username);
        }
        $password = "";
        if(isset($this->request->data['password']))
        {
            $username = $this->request->data['password'];
            error_log($username);
        }
       $data = array('Id'=>$userid,'Name'=>$username,'LoginDate'=>date('Y/m/d H:i:s'),'Password'=>$password);
       $userbasicdata = $this->Userbasicdata->newEntity();
       $userbasicdata = $this->Userbasicdata->patchEntity($userbasicdata,$data);
       $userbasicdata->updateAll(['Name' => $username],['LoginDate'=>date('Y/m/d H:i:s')],['Password'=>$password]);
       if($this->Userbasicdata->save($userbasicdata))
       {
           echo "1";
       }
       else 
       {
           echo "0";
       }
    }
}
