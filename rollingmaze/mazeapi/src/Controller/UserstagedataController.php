<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Userstagedata Controller
 *
 * @property \App\Model\Table\UserstagedataTable $Userstagedata
 *
 * @method \App\Model\Entity\Userstagedata[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserstagedataController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $userstagedata = $this->paginate($this->Userstagedata);

        $this->set(compact('userstagedata'));
    }

    /**
     * View method
     *
     * @param string|null $id Userstagedata id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userstagedata = $this->Userstagedata->get($id, [
            'contain' => []
        ]);

        $this->set('userstagedata', $userstagedata);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userstagedata = $this->Userstagedata->newEntity();
        if ($this->request->is('post')) {
            $userstagedata = $this->Userstagedata->patchEntity($userstagedata, $this->request->getData());
            if ($this->Userstagedata->save($userstagedata)) {
                $this->Flash->success(__('The userstagedata has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The userstagedata could not be saved. Please, try again.'));
        }
        $this->set(compact('userstagedata'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Userstagedata id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userstagedata = $this->Userstagedata->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userstagedata = $this->Userstagedata->patchEntity($userstagedata, $this->request->getData());
            if ($this->Userstagedata->save($userstagedata)) {
                $this->Flash->success(__('The userstagedata has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The userstagedata could not be saved. Please, try again.'));
        }
        $this->set(compact('userstagedata'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Userstagedata id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userstagedata = $this->Userstagedata->get($id);
        if ($this->Userstagedata->delete($userstagedata)) {
            $this->Flash->success(__('The userstagedata has been deleted.'));
        } else {
            $this->Flash->error(__('The userstagedata could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getUserstagedata()
    {
        error_log("getUserstagedata");
        $this->autoRender = false;

        $query = $this->Userstagedata->find('all');

        $json_array = \json_encode($query);

        echo $json_array;
    }

    public function addrecordUserstagedata()
    {
        error_log("addrecordUserstagedata");
        $this->autoRender = false;
    }

    public function updateUserstagedata()
    {
        error_log("updateUserstagedata");
        $this->autoRender = false;
    }
}
