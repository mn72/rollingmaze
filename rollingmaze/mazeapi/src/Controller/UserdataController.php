<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Userdata Controller
 *
 * @property \App\Model\Table\UserdataTable $Userdata
 *
 * @method \App\Model\Entity\Userdata[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserdataController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $userdata = $this->paginate($this->Userdata);

        $this->set(compact('userdata'));
    }

    /**
     * View method
     *
     * @param string|null $id Userdata id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userdata = $this->Userdata->get($id, [
            'contain' => []
        ]);

        $this->set('userdata', $userdata);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userdata = $this->Userdata->newEntity();
        if ($this->request->is('post')) {
            $userdata = $this->Userdata->patchEntity($userdata, $this->request->getData());
            if ($this->Userdata->save($userdata)) {
                $this->Flash->success(__('The userdata has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The userdata could not be saved. Please, try again.'));
        }
        $this->set(compact('userdata'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Userdata id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userdata = $this->Userdata->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userdata = $this->Userdata->patchEntity($userdata, $this->request->getData());
            if ($this->Userdata->save($userdata)) {
                $this->Flash->success(__('The userdata has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The userdata could not be saved. Please, try again.'));
        }
        $this->set(compact('userdata'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Userdata id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userdata = $this->Userdata->get($id);
        if ($this->Userdata->delete($userdata)) {
            $this->Flash->success(__('The userdata has been deleted.'));
        } else {
            $this->Flash->error(__('The userdata could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getUserdata()
  {
      error_log("getUserdata");
      $this->autoRender = false;

      $query = $this->Userdata->find('all');

      $json_array = json_encode($query);

      echo $json_array;
  }
  public function setUserdata()
  {
      error_log("setUserdata");
      $this->autoRender = false;
    //   $userid = "";
    //   if(isset($this->request->data['userid']))
    //   {
    //       $userid = $this->request->data['userid'];
    //       error_log($userid);
    //   }

    $postdata = $this->request->getData('Id');

    $userdatatable = $this->UserData->getTableLocator()->get('userdata');
    $targetrecode = $this->$userdatatable->get($postdata);
    // if(isset($userdata))
    // {
    //     echo $userdata;
    // }
      $latestcleartime = "";
      if(isset($this->request->data['latestcleartime']))
      {
          $latestcleartime = $this->request->data['latestcleartime'];
          error_log($latestcleartime);
      }
   
      $targetrecode->LoginDate = $targetrecode['LoginDate'] = date('Y/m/d H:i:s');
      $targetrecode->LatestClearTime = $targetrecode['LatestClearTime'] = latestcleartime;



    //  $data = array(
    //      'LoginDate'=>date('Y/m/d H:i:s'),
    //      'LatestclearTime'=>$latestcleartime);
    //  //$userdata = $this->Userdata->newEntity();
    //  $userdata = $this->Userdata->patchEntity($userdata,$data);
    //$this->Userdata->updateAll(['LoginDate'=>date('Y/m/d H:i:s')],['LatestClearTime'=>$latestcleartime]);
    //echo "updated!!";

    //  if($this->Userdata->save($userdata))
    //  {
    //      echo "1";
    //      echo "Success!!";
    //  }
    //  else 
    //  {
    //      echo "0";
    //      echo "Faild";
    //  }
  }
//   public function updateUserdata()
//   {
//       error_log("updateUserdata");
//       $this->autoRender = false;  
//     //   $table = $userdata->getUserdata();
//     //   $element = $table->get(12345678);
//     //   $element->LoginDate = date('Y/m/d H:i:s');


//      $data = array(
//          'LoginDate'=>date('Y/m/d H:i:s'),
//          'LatestclearTime'=>$latestcleartim);
//      $userdata = $this->Userdata->newEntity();
//      $userdata = $this->Userdata->patchEntity($userdata,$data);

//      if($this->Userdata->save($userdata))
//      {
//          echo "1";
//      }
//      else 
//      {
//          echo "0";
//      }
//   }
}