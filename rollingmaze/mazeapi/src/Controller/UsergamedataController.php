<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Usergamedata Controller
 *
 * @property \App\Model\Table\UsergamedataTable $Usergamedata
 *
 * @method \App\Model\Entity\Usergamedata[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsergamedataController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $usergamedata = $this->paginate($this->Usergamedata);

        $this->set(compact('usergamedata'));
    }

    /**
     * View method
     *
     * @param string|null $id Usergamedata id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usergamedata = $this->Usergamedata->get($id, [
            'contain' => []
        ]);

        $this->set('usergamedata', $usergamedata);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $usergamedata = $this->Usergamedata->newEntity();
        if ($this->request->is('post')) {
            $usergamedata = $this->Usergamedata->patchEntity($usergamedata, $this->request->getData());
            if ($this->Usergamedata->save($usergamedata)) {
                $this->Flash->success(__('The usergamedata has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The usergamedata could not be saved. Please, try again.'));
        }
        $this->set(compact('usergamedata'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Usergamedata id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $usergamedata = $this->Usergamedata->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usergamedata = $this->Usergamedata->patchEntity($usergamedata, $this->request->getData());
            if ($this->Usergamedata->save($usergamedata)) {
                $this->Flash->success(__('The usergamedata has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The usergamedata could not be saved. Please, try again.'));
        }
        $this->set(compact('usergamedata'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Usergamedata id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $usergamedata = $this->Usergamedata->get($id);
        if ($this->Usergamedata->delete($usergamedata)) {
            $this->Flash->success(__('The usergamedata has been deleted.'));
        } else {
            $this->Flash->error(__('The usergamedata could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getUsergamedata()
    {
        error_log("getUsergamedata");
        $this->autoRender = false;

        $query = $this->Usergamedata->find('all');

        $json_array = \json_encode($query);

        echo $json_array;
    }

    public function addrecordUsergamedata()
    {
        error_log("addrecordUsergamedata");
        $this->autoRender = false;
    }

    public function updateUsergamedata()
    {
        error_log("updateUsergamedata");
        $this->autoRender = false;
    }
}
