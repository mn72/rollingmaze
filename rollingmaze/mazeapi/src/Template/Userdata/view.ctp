<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Userdata $userdata
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Userdata'), ['action' => 'edit', $userdata->Id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Userdata'), ['action' => 'delete', $userdata->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $userdata->Id)]) ?> </li>
        <li><?= $this->Html->link(__('List Userdata'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Userdata'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userdata view large-9 medium-8 columns content">
    <h3><?= h($userdata->Id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= h($userdata->Id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('LatestClearTime') ?></th>
            <td><?= h($userdata->LatestClearTime) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('LoginDate') ?></th>
            <td><?= h($userdata->LoginDate) ?></td>
        </tr>
    </table>
</div>
