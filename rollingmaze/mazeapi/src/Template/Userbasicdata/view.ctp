<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Userbasicdata $userbasicdata
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Userbasicdata'), ['action' => 'edit', $userbasicdata->Id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Userbasicdata'), ['action' => 'delete', $userbasicdata->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $userbasicdata->Id)]) ?> </li>
        <li><?= $this->Html->link(__('List Userbasicdata'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Userbasicdata'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userbasicdata view large-9 medium-8 columns content">
    <h3><?= h($userbasicdata->Id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= h($userbasicdata->Id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($userbasicdata->Name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($userbasicdata->Password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('RegistrationDate') ?></th>
            <td><?= h($userbasicdata->RegistrationDate) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('LoginDate') ?></th>
            <td><?= h($userbasicdata->LoginDate) ?></td>
        </tr>
    </table>
</div>
