<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Userbasicdata[]|\Cake\Collection\CollectionInterface $userbasicdata
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Userbasicdata'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userbasicdata index large-9 medium-8 columns content">
    <h3><?= __('Userbasicdata') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('Id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('RegistrationDate') ?></th>
                <th scope="col"><?= $this->Paginator->sort('LoginDate') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Password') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userbasicdata as $userbasicdata): ?>
            <tr>
                <td><?= h($userbasicdata->Id) ?></td>
                <td><?= h($userbasicdata->Name) ?></td>
                <td><?= h($userbasicdata->RegistrationDate) ?></td>
                <td><?= h($userbasicdata->LoginDate) ?></td>
                <td><?= h($userbasicdata->Password) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userbasicdata->Id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userbasicdata->Id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userbasicdata->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $userbasicdata->Id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
