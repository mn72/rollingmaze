<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Userbasicdata $userbasicdata
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $userbasicdata->Id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $userbasicdata->Id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Userbasicdata'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="userbasicdata form large-9 medium-8 columns content">
    <?= $this->Form->create($userbasicdata) ?>
    <fieldset>
        <legend><?= __('Edit Userbasicdata') ?></legend>
        <?php
            echo $this->Form->control('Name');
            echo $this->Form->control('RegistrationDate');
            echo $this->Form->control('LoginDate');
            echo $this->Form->control('Password');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
