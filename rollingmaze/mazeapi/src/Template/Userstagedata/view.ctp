<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Userstagedata $userstagedata
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Userstagedata'), ['action' => 'edit', $userstagedata->UserId]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Userstagedata'), ['action' => 'delete', $userstagedata->UserId], ['confirm' => __('Are you sure you want to delete # {0}?', $userstagedata->UserId)]) ?> </li>
        <li><?= $this->Html->link(__('List Userstagedata'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Userstagedata'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userstagedata view large-9 medium-8 columns content">
    <h3><?= h($userstagedata->UserId) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('UserId') ?></th>
            <td><?= h($userstagedata->UserId) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('LatestClearTime') ?></th>
            <td><?= h($userstagedata->LatestClearTime) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('FastestClearTime') ?></th>
            <td><?= h($userstagedata->FastestClearTime) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('StageNum') ?></th>
            <td><?= $this->Number->format($userstagedata->StageNum) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ClearCount') ?></th>
            <td><?= $this->Number->format($userstagedata->ClearCount) ?></td>
        </tr>
    </table>
</div>
