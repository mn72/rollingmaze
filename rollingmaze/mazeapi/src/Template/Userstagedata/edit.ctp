<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Userstagedata $userstagedata
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $userstagedata->UserId],
                ['confirm' => __('Are you sure you want to delete # {0}?', $userstagedata->UserId)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Userstagedata'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="userstagedata form large-9 medium-8 columns content">
    <?= $this->Form->create($userstagedata) ?>
    <fieldset>
        <legend><?= __('Edit Userstagedata') ?></legend>
        <?php
            echo $this->Form->control('StageNum');
            echo $this->Form->control('LatestClearTime');
            echo $this->Form->control('FastestClearTime');
            echo $this->Form->control('ClearCount');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
