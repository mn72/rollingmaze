<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Userstagedata[]|\Cake\Collection\CollectionInterface $userstagedata
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Userstagedata'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userstagedata index large-9 medium-8 columns content">
    <h3><?= __('Userstagedata') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('UserId') ?></th>
                <th scope="col"><?= $this->Paginator->sort('StageNum') ?></th>
                <th scope="col"><?= $this->Paginator->sort('LatestClearTime') ?></th>
                <th scope="col"><?= $this->Paginator->sort('FastestClearTime') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ClearCount') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userstagedata as $userstagedata): ?>
            <tr>
                <td><?= h($userstagedata->UserId) ?></td>
                <td><?= $this->Number->format($userstagedata->StageNum) ?></td>
                <td><?= h($userstagedata->LatestClearTime) ?></td>
                <td><?= h($userstagedata->FastestClearTime) ?></td>
                <td><?= $this->Number->format($userstagedata->ClearCount) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userstagedata->UserId]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userstagedata->UserId]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userstagedata->UserId], ['confirm' => __('Are you sure you want to delete # {0}?', $userstagedata->UserId)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
