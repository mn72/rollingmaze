<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usergamedata $usergamedata
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Usergamedata'), ['action' => 'edit', $usergamedata->UserId]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Usergamedata'), ['action' => 'delete', $usergamedata->UserId], ['confirm' => __('Are you sure you want to delete # {0}?', $usergamedata->UserId)]) ?> </li>
        <li><?= $this->Html->link(__('List Usergamedata'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Usergamedata'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="usergamedata view large-9 medium-8 columns content">
    <h3><?= h($usergamedata->UserId) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('UserId') ?></th>
            <td><?= h($usergamedata->UserId) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Difficulty') ?></th>
            <td><?= h($usergamedata->Difficulty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('StageClearStatus') ?></th>
            <td><?= $this->Number->format($usergamedata->StageClearStatus) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Speed') ?></th>
            <td><?= $this->Number->format($usergamedata->Speed) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('SkinIdentification') ?></th>
            <td><?= $this->Number->format($usergamedata->SkinIdentification) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Coin') ?></th>
            <td><?= $this->Number->format($usergamedata->Coin) ?></td>
        </tr>
    </table>
</div>
