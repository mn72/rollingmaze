<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usergamedata[]|\Cake\Collection\CollectionInterface $usergamedata
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Usergamedata'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="usergamedata index large-9 medium-8 columns content">
    <h3><?= __('Usergamedata') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('UserId') ?></th>
                <th scope="col"><?= $this->Paginator->sort('StageClearStatus') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Difficulty') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Speed') ?></th>
                <th scope="col"><?= $this->Paginator->sort('SkinIdentification') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Coin') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usergamedata as $usergamedata): ?>
            <tr>
                <td><?= h($usergamedata->UserId) ?></td>
                <td><?= $this->Number->format($usergamedata->StageClearStatus) ?></td>
                <td><?= h($usergamedata->Difficulty) ?></td>
                <td><?= $this->Number->format($usergamedata->Speed) ?></td>
                <td><?= $this->Number->format($usergamedata->SkinIdentification) ?></td>
                <td><?= $this->Number->format($usergamedata->Coin) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $usergamedata->UserId]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $usergamedata->UserId]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $usergamedata->UserId], ['confirm' => __('Are you sure you want to delete # {0}?', $usergamedata->UserId)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
