<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usergamedata $usergamedata
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $usergamedata->UserId],
                ['confirm' => __('Are you sure you want to delete # {0}?', $usergamedata->UserId)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Usergamedata'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="usergamedata form large-9 medium-8 columns content">
    <?= $this->Form->create($usergamedata) ?>
    <fieldset>
        <legend><?= __('Edit Usergamedata') ?></legend>
        <?php
            echo $this->Form->control('StageClearStatus');
            echo $this->Form->control('Difficulty');
            echo $this->Form->control('Speed');
            echo $this->Form->control('SkinIdentification');
            echo $this->Form->control('Coin');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
