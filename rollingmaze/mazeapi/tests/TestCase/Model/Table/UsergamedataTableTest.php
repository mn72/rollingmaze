<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsergamedataTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsergamedataTable Test Case
 */
class UsergamedataTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UsergamedataTable
     */
    public $Usergamedata;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Usergamedata'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Usergamedata') ? [] : ['className' => UsergamedataTable::class];
        $this->Usergamedata = TableRegistry::getTableLocator()->get('Usergamedata', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Usergamedata);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
