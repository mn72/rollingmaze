<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserstagedataTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserstagedataTable Test Case
 */
class UserstagedataTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserstagedataTable
     */
    public $Userstagedata;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Userstagedata'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Userstagedata') ? [] : ['className' => UserstagedataTable::class];
        $this->Userstagedata = TableRegistry::getTableLocator()->get('Userstagedata', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Userstagedata);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
