<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserbasicdataTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserbasicdataTable Test Case
 */
class UserbasicdataTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserbasicdataTable
     */
    public $Userbasicdata;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Userbasicdata'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Userbasicdata') ? [] : ['className' => UserbasicdataTable::class];
        $this->Userbasicdata = TableRegistry::getTableLocator()->get('Userbasicdata', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Userbasicdata);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
