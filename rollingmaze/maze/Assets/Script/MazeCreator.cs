﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeCreator : MonoBehaviour
{
    /// <summary>
    /// 読み込んだCSVデータを格納するための配列
    /// 初期値はnull
    /// int[,]はint型の二次元配列
    /// </summary>
    public int[,] mazeData = null;

    /// <summary>
    /// ステージ番号
    /// </summary>
    //public int stageNum;

    int rnd;


    [SerializeField] private GameObject floor = default;
    [SerializeField] private GameObject perimeterWall = default;
    [SerializeField] private GameObject player = default;
    [SerializeField] private GameObject goal = default;

    // Start is called before the first frame update
    void Start()
    {
        rnd = Random.Range(1, 3);
        // データパスを設定
        // Assetフォルダ以下の階層を書くので、/で階層を区切り、csvデータ名まで書く（拡張子：csvも忘れずに）
        //string dataPath = "/CSV/stage6.csv";
        string dataPath1 = "/Resources/Stage/stage" + rnd + ".csv";

        // CSV読み込みクラスのインスタンス化
        CSVReder csv = new CSVReder(dataPath1);

        // CSVファイルを呼び出す
        mazeData = csv.Read();

        // 縦の要素数
        int h = 25;
        // 横の要素数
        int w = 25;
        // 座標
        Vector3 pos = Vector3.zero;
        // 角度
        Quaternion rot = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);

        for (int i = 0; i < h; i++)
        {
            for (int j = 0; j < w; j++)
            {
                switch (mazeData[i, j])
                {
                    // 配列の値が０なら
                    case 0:
                        // 何も配置しない
                        break;
                    // 配列の値が１なら
                    case 1:
                        // 座標を設定して
                        pos.x = j;
                        pos.y = -1.0f;
                        pos.z = i;
                        Instantiate(floor, pos, rot);
                        break;
                    // 配列の値が２なら
                    case 2:
                        // 座標を設定して
                        pos.x = j;
                        pos.y = -1.0f;
                        pos.z = i;
                        Instantiate(perimeterWall, pos, rot);
                        break;
                    // 配列の値が３なら
                    case 3:
                        // 座標を設定して
                        pos.x = j;
                        pos.y = -1.0f;
                        pos.z = i;
                        Instantiate(floor, pos, rot);
                        pos.y = 0.0f;
                        Instantiate(player, pos, rot);
                        break;
                    // 配列の値が４なら
                    case 4:
                        // 座標を設定して
                        pos.x = j;
                        pos.y = 0.5f;
                        pos.z = i;
                        // ゴールのブロックを配置する
                        Instantiate(goal, pos, rot);
                        break;
                    // 上記以外の数値が入っていたら
                    default:
                        // 何も配置しない
                        break;
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
