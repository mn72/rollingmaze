﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Result : MonoBehaviour
{

    public Text cleartime = default;

    public void OnClickReturntoStageselectButton()
    {
        SceneManager.LoadScene("Title");
    }

    public void OnClickPlayAgainButton()
    {
        SceneManager.LoadScene("Stage1");
    }
    public void OnClickGameQuitButton()
    {
        Application.Quit();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        cleartime.text = PlayerController.cleartime.ToString("0000") + "秒";
        
    }
}
