﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour
{

    public void OnClickBeginningStartButton()
    {
        SceneManager.LoadScene("UserRegistration");
    }
    
    public void OnClickContinueStartButton()
    {
        //GetComponent<DataManager>().GetJsonFromWeb();
        SceneManager.LoadScene("Stage1");
    }
    public void OnClickGameQuitButton()
    {
        Application.Quit();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
