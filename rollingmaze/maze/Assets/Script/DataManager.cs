﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DataManager : MonoBehaviour
{

    private static List<Data> dataLists;

    public string TextClearTime { set; get; }

    public void GetJsonFromWeb()
    {
        Debug.Log("wait....");
        GetJsonFromWebRequest();
    }

    public static void OnClickShowMemberList()
    {
        string sStrOutput = "";

        if (null == dataLists)
        {
            sStrOutput = "no list !";
        }
        else
        {
            //リストの内容を表示
            foreach (Data memberOne in dataLists)
            {
                sStrOutput += $"ID:{memberOne.ID} LoginDate:{memberOne.LoginDate} Cleartime:{memberOne.LatestClearTime} \n \n";
            }
        }

        Debug.Log(sStrOutput);
    }
    /// <summary>
    /// Gets the json from www.
    /// </summary>
    private void GetJsonFromWebRequest()
    {
        // API を呼んだ際に想定されるレスポンス
        // [{"name":"\u3072\u3068\u308a\u3081","age":123,"hobby":"\u30b4\u30eb\u30d5"},{"name":"\u3075\u305f\u308a\u3081","age":25,"hobby":"walk"},{"name":"\u3055\u3093\u306b\u3093\u3081","age":77,"hobby":"\u5c71"}]
        //

        // Wwwを利用して json データ取得をリクエストする
        StartCoroutine(
            DownloadJson(
                CallbackWebRequestSuccess, // APIコールが成功した際に呼ばれる関数を指定
                CallbackWebRequestFailed // APIコールが失敗した際に呼ばれる関数を指定
            )
        );
    }

    /// <summary>
    /// Callbacks the www success.
    /// </summary>
    /// <param name="response">Response.</param>
    private void CallbackWebRequestSuccess(string response)
    {
        //Json の内容を MemberData型のリストとしてデコードする。
        dataLists = DataModel.DeserializeFromJson(response);

        //memberList ここにデコードされたメンバーリストが格納される。

        Debug.Log("CallbackWebReqestSuccess!");
    }


    /// <summary>
    /// Callbacks the www failed.
    /// </summary>
    private void CallbackWebRequestFailed()
    {
        // jsonデータ取得に失敗した
        Debug.Log("WebRequest Failed");
    }

    /// <summary>
    /// Downloads the json.
    /// </summary>
    /// <returns>The json.</returns>
    /// <param name="cbkSuccess">Cbk success.</param>
    /// <param name="cbkFailed">Cbk failed.</param>
    private IEnumerator DownloadJson(Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        UnityWebRequest www = UnityWebRequest.Get("http://localhost/mazeapi/userdata/getUserdata");
        yield return www.SendWebRequest();
        if (www.error != null)
        {
            //レスポンスエラーの場合
            Debug.LogError(www.error);
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (www.isDone)
        {
            // リクエスト成功の場合
            Debug.Log($"Success:{www.downloadHandler.text}");
            if (null != cbkSuccess)
            {
                cbkSuccess(www.downloadHandler.text);
            }
        }
    }
    private void SetJsonFromWWW()
    {
        string targetURL = "http://localhost/mazeapi/userdata/setUserdata";


        string time = TextClearTime;

        StartCoroutine(SetData(targetURL, time, CallbackAPISuccess, CallbackWebRequestFailed));
    }


    private void CallbackAPISuccess()
    {
       Debug.Log("CallbackAPISuccess!");
    }

    //private void CallbackAPISuccess()
    //{
    //    _displayField.text = "送信完了！";
    //}

    private IEnumerator SetData(string url, string time, Action cbkSuccess = null, Action cbkFailed = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("LatestClearTime",time);
        

        var webRequest = UnityWebRequest.Post(url, form);
        webRequest.timeout = 5;

        yield return webRequest.SendWebRequest();

        if (webRequest.error != null)
        {
            Debug.LogError(webRequest.error);

            if (cbkFailed != null)
            {
                cbkFailed();
            }
        }
        else if (webRequest.isDone)
        {
            if (cbkSuccess != null)
            {
                cbkSuccess();
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
