﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField] Text timerText = default;
    public static float second;

    // Start is called before the first frame update
    void Start()
    {
        second = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        second += Time.deltaTime;
        timerText.text = " " + second.ToString("0000");
    }
}
