﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// CSVデータを読み込むクラス
/// </summary>
public class CSVReder
{
    /// <summary>
    /// 読み込みたいCSVファイルのパス
    /// </summary>
    public string dataPath = null;

    /// <summary>
    /// 列数の要素数
    /// 定数
    /// </summary>
    static int height = 25;

    /// <summary>
    /// 行数の要素数
    /// 定数
    /// </summary>
    static int width = 25;

    /// <summary>
    /// CSVデータの読み込み関数
    /// </summary>
    /// <param name="path"> 読み込みたいCSVデータのパス </param>
    /// <returns> int型の二次元配列で返す </returns>
    public int[,] Read(string path)
    {
        // 返り値の二次元配列
        int[,] returnAsIntData = new int [height,width];
        // 何行目を読み込んでるか
        int rowCount = 0;

        // StreamReader型の変数streamに読み込む
        // ※Application.dataPathはプロジェクトデータのAssetフォルダまでのアクセスパスの事
        StreamReader stream = new StreamReader(Application.dataPath + path);

        //// ファイルの中身をすべて読み込む
        //string strStream = stream.ReadToEnd();

        // Peek()で次の文字を取得できる間
        while(stream.Peek() > -1)
        {
            // 一行ずつ取り出す
            string line = stream.ReadLine();

            // 区切る文字（カンマ）を設定する
            char[] spliter = new char[1] { ',' };

            // 区切る文字（カンマ）ごと分ける
            string[] lines = line.Split(spliter);

            // 列数分だけ回す
            for(int i = 0; i < lines.Length; i++)
            {
                // 一時保管の変数
                int tmp;

                // 一つずつ文字列から数値型に変換したか確認する
                if (int.TryParse(lines[i], out tmp) )
                {
                    // 配列に一つずつ格納する
                    returnAsIntData[rowCount, i] = tmp;
                    //Debug.Log(rowCount + " - " + i + " : " + tmp);
                }
                else
                {
                    // 中のデータがint型以外のデータが入っていた
                }
            }
            // 行数をカウントする
            rowCount++;
        }

        // ファイルの読み込みが終わったので、StreamReaderを閉じる
        stream.Close();

        // 返り値
        return returnAsIntData;
    }

    /// <summary>
    /// CSVデータの読み込み関数を呼ぶ関数（オーバーロード）
    /// </summary>
    /// <returns> int型の二次元配列で返す </returns>
    public int[,] Read()
    {
        return Read(dataPath);
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="path"> 読み込みたいCSVファイルまでのパス </param>
    public CSVReder(string path)
    {
        dataPath = path;
    }

 //   // Use this for initialization
 //   void Start ()
 //   {
 //   }

 //   // Update is called once per frame
 //   void Update ()
 //   {
 //   }
}
