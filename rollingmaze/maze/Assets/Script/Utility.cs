﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : MonoBehaviour
{
    GameObject instance;

    private void FinishGame()
    {
        //if(Application.platform == RuntimePlatform.Android)
        //{
        //    if(Input.GetKeyDown(KeyCode.Escape))
        //    {
        //        Application.Quit();
        //        return;
        //    }

        //}
        Input.backButtonLeavesApp = true;
    }
    // Start is called before the first frame update
    void Start()
    {
        if(instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this.gameObject;
        }

        DontDestroyOnLoad(this.gameObject);
    }
}
