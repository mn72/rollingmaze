﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data : MonoBehaviour
{
    public string LatestClearTime { set; get; }
    public string LoginDate { get; set; }
    public string ID { get; set; }

    public Data()
    {
        LatestClearTime = "";
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
