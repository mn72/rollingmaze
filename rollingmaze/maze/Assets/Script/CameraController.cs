﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private GameObject player;
    private Vector3 camerapos;
    // Start is called before the first frame update
    void Start()
    {
        camerapos = new Vector3(player.transform.position.x, 10.0f, player.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        camerapos.x = player.transform.position.x;
        camerapos.z = player.transform.position.z;
        transform.position = camerapos;
    }
}
