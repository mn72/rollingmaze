﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public static float cleartime = 0.0f;
    private Rigidbody rb;

    private float speed = 100.0f;
    private float Speed
    {
        set
        {
            speed = value;
        }
        get
        {
            return speed;
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Goal"))
        {
            cleartime = Timer.second;
            SceneManager.LoadScene("Result");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Application.isEditor)
        {
            // 水平軸と垂直軸からの入力を記録
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");
            Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
            rb.AddForce(movement * speed);
        }
        else
        {
            var inclination = Input.acceleration;
            inclination.Normalize();
            Vector3 direction = new Vector3(inclination.x, 0.0f, inclination.z);
            direction.x *= Time.deltaTime;
            direction.z *= Time.deltaTime;
            rb.AddForce(direction * speed);
        }
        
      }
}
